package com.test.output;

import java.util.ArrayList;

import com.test.basicInfo.CustomerCurrentHealth;
import com.test.basicInfo.CustomerHabits;
import com.test.basicInfo.CustomerInfo;
import com.test.logic.CalculateInsurance;

public class FinalResult {
	
	public static void main(String args[]) {
	
	CustomerInfo customerInfo=new CustomerInfo("Norman Gomes","male",34);
	
	CustomerHabits customerHabits=new CustomerHabits("No","Yes","Yes","No");
	
	ArrayList<String> customerHabitsList=new ArrayList<String>();
	
	customerHabitsList.add(customerHabits.getDailyExercise());
	customerHabitsList.add(customerHabits.getAlcohol());
	customerHabitsList.add(customerHabits.getDrugs());
	customerHabitsList.add(customerHabits.getSmoking());
	
	CustomerCurrentHealth customerCurrentHealth=new CustomerCurrentHealth("No","No","No","Yes");
	
	ArrayList<String> customerCurrentHealthList=new ArrayList<String>();
	customerCurrentHealthList.add(customerCurrentHealth.getBloodPressure());
	customerCurrentHealthList.add(customerCurrentHealth.getBloodSugar());
	customerCurrentHealthList.add(customerCurrentHealth.getHyperTension());
	customerCurrentHealthList.add(customerCurrentHealth.getOverWeight());
	
	
	CalculateInsurance calculateInsurance=new CalculateInsurance(customerInfo,customerHabitsList,customerCurrentHealthList);
	
	int basePremium=calculateInsurance.basePremiumCalculation(customerInfo.getAge());
	System.out.println("basePremium"+basePremium);
	
	int insuranceBasedOnGender=calculateInsurance.insuranceBasedOnGender(customerInfo.getGender(),basePremium);
	
	System.out.println("basePremium + insuranceBasedOnGender"+insuranceBasedOnGender);
	int insuranceBasedOnCondition=calculateInsurance.getInsuranceBasedCondition(customerCurrentHealthList,insuranceBasedOnGender);
	
	System.out.println("basePremium + insuranceBasedOnGender + insuranceBasedOnCondition"+insuranceBasedOnCondition);
	int insuranceBasedOnHabits=calculateInsurance.getInsuranceBasedHabits(customerHabits.getDailyExercise(),insuranceBasedOnCondition,customerHabits.getAlcohol(),customerHabits.getDrugs(),customerHabits.getSmoking());
	
	System.out.println("basePremium + insuranceBasedOnGender + insuranceBasedOnCondition+insuranceBasedOnHabits"+insuranceBasedOnHabits);
	

}
}
