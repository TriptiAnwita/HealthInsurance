package com.test.logic;

import java.util.ArrayList;

import com.test.basicInfo.CustomerCurrentHealth;
import com.test.basicInfo.CustomerHabits;
import com.test.basicInfo.CustomerInfo;

public class CalculateInsurance {

	CustomerInfo customerInfo;
	ArrayList<String> customerHabitsList;
	ArrayList<String> customerCurrentHealthList;

	static int baseInsurance = 5000;
	static int calInsurance;

	static int finalInsurance;

	public CalculateInsurance(CustomerInfo customerInfo, ArrayList<String> customerHabitsList,
			ArrayList<String> customerCurrentHealthList) {
		super();
		this.customerInfo = customerInfo;
		this.customerHabitsList = customerHabitsList;
		this.customerCurrentHealthList = customerCurrentHealthList;
	}

	public int basePremiumCalculation(int age) {

		if (age < 18) {

			calInsurance = baseInsurance;
		}

		else if (age > 18 && age < 25) {

			calInsurance = (int) ((baseInsurance) + (0.1 * baseInsurance));
		}

		else if (age >= 25 && age <= 40) {

			for (int i = 25; i < 40; i = i + 5) {

				calInsurance = (int) ((baseInsurance) + (0.1 * baseInsurance));
				baseInsurance = calInsurance;
			}
		} else if (age >= 40) {
			for (int i = 25; i < 40; i = i + 5) {

				calInsurance = (int) ((baseInsurance) + (0.2 * baseInsurance));
				baseInsurance = calInsurance;
			}
		}
		return calInsurance;

	}

	public int insuranceBasedOnGender(String gender, int basePremium) {

		calInsurance = basePremium;
		if (gender.equals("male")) {
			calInsurance = (int) (calInsurance + (0.02 * calInsurance));
		}
		return calInsurance;
	}

	public int getInsuranceBasedCondition(ArrayList<String> customerCurrentHealthList,
			int insuranceBasedOnGender) {
		calInsurance = insuranceBasedOnGender;
		int count = 0;

		for (String cond : customerCurrentHealthList) {

			if (cond.equals("Yes")) {

				count = count + 1;

			}

			calInsurance = (int) (calInsurance + (0.01 * calInsurance * count));

		}

		return calInsurance;

	}

	public int getInsuranceBasedHabits( String dailyExercise,
			int insuranceBasedOnCondition, String smoking, String drugs, String alcohol) {

		calInsurance = insuranceBasedOnCondition;
	
		int count= 0;
		int goodcount=0;
		int badcount=-1;
		
		
		if (dailyExercise.equals("Yes")) {
			goodcount=1;
	
			

		}
		
		else if (smoking.equals("Yes")) {
			badcount=badcount-1;
	
			

		}

		else if (drugs.equals("Yes")) {
			badcount=badcount-1;
	
			

		}
		else if (alcohol.equals("Yes")) {
			badcount=badcount-1;
	
			

		}
		
		
		count=goodcount+badcount;
		System.out.println(goodcount);
		System.out.println(badcount);
		
		System.out.println(count);
		
			calInsurance = (int) (calInsurance + (0.03 * calInsurance * count));

		
		
	


		return calInsurance;
	}

}
