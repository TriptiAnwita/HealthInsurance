package com.test.basicInfo;

public class CustomerCurrentHealth {

	private String hyperTension;
	private String bloodPressure;
	private String bloodSugar;
	private String overWeight;

	public CustomerCurrentHealth(String hyperTension, String bloodPressure, String bloodSugar, String overWeight) {
		super();
		this.hyperTension = hyperTension;
		this.bloodPressure = bloodPressure;
		this.bloodSugar = bloodSugar;
		this.overWeight = overWeight;
	}

	public String getHyperTension() {
		return hyperTension;
	}

	public void setHyperTension(String hyperTension) {
		this.hyperTension = hyperTension;
	}

	public String getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public String getBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public String getOverWeight() {
		return overWeight;
	}

	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}

}
